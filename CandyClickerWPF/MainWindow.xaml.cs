﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Engine;
using Dragablz.Dockablz;
using Dragablz.Properties;
using Dragablz.Themes;
using Dragablz.Referenceless;
using Dragablz.Core;
using Dragablz.Converters;
using Dragablz;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Concurrent;
using System.Runtime.Serialization;
using System.Collections.Specialized;
using System.Reactive;

namespace CandyClickerWPF
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CandyClicker : Window
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
            }
        }


        private  ObservableConcurrentDictionary<string, decimal> _myItems = GameStatus.Singleton.PlayerInventory;
        public ObservableConcurrentDictionary<string, decimal> myItems
        {
            get { return _myItems; }
            set 
            { 
                myItems = value;
                onPropertyChanged(this, "myItems");
            }
        }
        


        public CandyClicker()
        {

            InitializeComponent();
            //add load option
            //this.WindowStyle = WindowStyle.None;
            LoadStuff();

            ListOfThings.DataContext = myItems;
            //MyListBox.DataContext = this;
            //myItems.PropertyChanged += myItems_PropertyChanged;
            //myItems.CollectionChanged += myItems_CollectionChanged;
            
            
        }

        void myItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
        }

        void myItems_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }


        
        private void candyButton_Click(object sender, RoutedEventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("Candy", 1);
        }
        private void chocoButton_Click(object sender, RoutedEventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("ChocoChips", 1);
        }
        private void sugarButton_Click(object sender, RoutedEventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("SugarCube", 1);
        }


        private void ExitGame(object sender, RoutedEventArgs e)
        {
            // Game saves
            //would you like to exit the game?
            //Application.Current.Shutdown();
        }

        private void TabablzControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // idk what this is but uh
        }

        private void LoadStuff()
        {

        }

        private void ListOfThings_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            
        }

    }
}