﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Engine
{
    public class Item
    {

        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }
        public decimal? MaxInvQuantity { get; set; }
        public string ItemGenerated { get; set; }
        public string PriceType { get; set; }

        public Dictionary<string, decimal> RequiredSpecialItem { get; set; }
        public Dictionary<string, decimal> PurchaseRequirements { get; set; }
        public Dictionary<string, decimal> RequiredMachine { get; set; }
        public decimal? InitialPrice { get; set; }



        public Item(JObject jRef)
        {
            Parse(jRef);
        }
        public void Parse(JObject jRef)
        {
            ID = jRef.GetPropertyString("ID");
            Name = jRef.GetPropertyString("Name");
            Description = jRef.GetPropertyString("Description");
            Class = jRef.GetPropertyString("Class");
            MaxInvQuantity = jRef.GetPropertyDecimal("MaxInvQuantity");
            InitialPrice = jRef.GetPropertyDecimal("InitialPrice");
            PurchaseRequirements = jRef.ParsePropertyToDecimalDictionary("PurchaseRequirement");
            RequiredMachine = jRef.ParsePropertyToDecimalDictionary("RequiredMachine");
            ItemGenerated = jRef.GetPropertyString("ItemGenerated");
            PriceType = jRef.GetPropertyString("PriceType");
            RequiredSpecialItem = jRef.ParsePropertyToDecimalDictionary("RequiredSpecialItem");
        }
        public static Item ParseJToken(JToken jRef)
        {
            Item i = null;
            if (jRef.Type == JTokenType.Object)
            {
                i = new Item((JObject)jRef);
            }
            return i;
        }


    }
}
