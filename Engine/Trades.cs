﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;


namespace Engine
{
    public class Trade
    {
        public Dictionary<string, decimal> RequiredItems { get; set; }
        public Dictionary<string, decimal> RewardItems { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string MenuText { get; set; }



        public Trade(string id, string name)
        {
            RequiredItems = new Dictionary<string, decimal>();
            RewardItems = new Dictionary<string, decimal>();
        }

        public Trade(JObject jRef)
        {
            Parse(jRef);
        }
        public void Parse(JObject jRef)
        {
            ID = jRef.GetPropertyString("ID");
            Name = jRef.GetPropertyString("Name");
            MenuText = jRef.GetPropertyString("MenuText");
            RequiredItems = jRef.ParsePropertyToDecimalDictionary("RequiredItems");
            RewardItems = jRef.ParsePropertyToDecimalDictionary("RewardItems");
        }
        public static Trade ParseToken(JToken jRef)
        {
            Trade i = null;
            if (jRef.Type == JTokenType.Object)
            {
                i = new Trade((JObject)jRef);
            }
            return i;
        }


    }
}
