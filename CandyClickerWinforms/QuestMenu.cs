﻿using Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class MarketMenu : Form
    {
        public List<MarketMenu> myQuests = new List<MarketMenu>();
        List<Button> btnTrade = new List<Button>();

        public MarketMenu()
        {
            InitializeComponent();
            AddButton();
            CheckTrade.Start();
        }
        private void Market_Load(object sender, EventArgs e)
        {
            // ...?
        }
        private void AddButton()
        {
            btnTrade.Add(GrandmaStand);
            btnTrade.Add(AuntieStand);
            btnTrade.Add(ChocoStand);
            btnTrade.Add(HoarderStand);
            btnTrade.Add(SugarFrenzy);

            foreach (Button btn in btnTrade)
            {
                btn.Enabled = false;
                btn.Visible = true;
            }


            
        }
        private void ClickTrade(object sender)
        {
            Button tsiClicked = sender as Button;
            string QuestID = (string)tsiClicked.Name;
            Trade myQuest = GameStatus.Singleton.FindTradeID(QuestID);

            if (GameStatus.Singleton.CanCompleteTrade(myQuest) == true)
            {
                GameStatus.Singleton.CompleteTheTrade(myQuest);
            }
            else
            {
                NotEnoughStuff();
            }
        }



        private void NotEnoughStuff()
        {
            MessageBox.Show("Not Enough Materials!");
        }


        private void ClickOnTrade(object sender, MouseEventArgs e)
        {
            if (e is System.Windows.Forms.MouseEventArgs)
            {
                Button myButton = sender as Button;
                ClickTrade(myButton);
            }
        }

        private void ClickOnTrade(object sender, EventArgs e)
        {
            if (e is System.Windows.Forms.MouseEventArgs)
            {
                Button myButton = sender as Button;
                myButton.Enabled = false;
            }
        }



        /* Trade Button workaround using a timer:
         * After each interval it will go through the button list and the trade dictionary
         * if the CanCompleteTrade function is true, and the ID of the trade is equal to that of one of the buttons' tag
         * it'll get that button and enable it and also ensure that it is visible.
         * otherwise, it will disable the button until the next interval, where it'll check everything again.
         * 
         * I'd rather this whole ordeal work with the Trade function, doing this exact same thing
         * 
         */ 
        private void CheckTrade_Tick(object sender, EventArgs e) // Timer to keep track of each trade button. This is a workaround for the trade event because the button is somehow kept at false visibility no matter what happens. This workaround works. 
        {

            List<Button> myBtn = btnTrade; // Gets the list of buttons in the form (5)
            Dictionary<string, Trade> myTrades = GameStatus.Singleton.AllTrades; // gets all the trades possible in the game (5)

            foreach (Button btn in myBtn) // for reach button...
            {
                btn.Enabled = false;
                foreach (KeyValuePair<string, Trade> kvp in myTrades) // and for each trade...
                {
                    if ((string)btn.Tag == (string)kvp.Value.ID && GameStatus.Singleton.CanCompleteTrade(kvp.Value)) // if the trade can be completed and the trade ID is equal to the button's Tag
                    {
                            btn.Enabled = true; // enable the button
                            btn.Visible = true; // make sure the button is visible
                            Program.bClicker.HeyIHaveATradeReady(kvp.Value);
                    }
                }
            }
        }



        /*How I want this to work:
         * Fired from the QuestReady Event along with another function on the main form (Basic Clicker). The other function enables a popup balloon that'll notify the player of a trade. This function is also broken.
         * When it gets to this function, it'll grab the list of the buttons on the Quest form and find which button the trade is associated with
         * This happens by matching the Trade's ID to the button's Tag OR Name (they're the same)
         * When it finds a match, I want it to enable the button and ensure that it is visible.
         * With the previous version of this, the visibility of the button was permanently false. I need a workaround for that. 
         * 
         */
        public void EnableTheTradeButton(Trade theTrade) // Kept here so the Trade Event we made won't give any errors 
        {
            // Gets the button using the Trade's ID because the Button's Tag and Name are equal to that
            // Enables the button and makes it visible.
        }





























































        // ATTEMPT 1 -- doesn't work. I just wanted to try out a switch case but it doesn't work for some odd reason. I don't like it either because it's so long.

        //public void EnableTheTradeButton(Trade theTrade)
        //{
        //    string myTradeID = theTrade.ID; // Gets the ID of the trade
        //    switch (myTradeID) // Matches the ID to the button 
        //    {
        //        case "GrandmaStand":
        //            {
        //                GrandmaStand.Enabled = true;
        //                GrandmaStand.Visible = true;
        //                break;
        //            }
        //        case "AuntieStand":
        //            {
        //                AuntieStand.Enabled = true;
        //                AuntieStand.Visible = true;
        //                break;
        //            }
        //        case "HoarderStand":
        //            {
        //                HoarderStand.Enabled = true;
        //                HoarderStand.Visible = true;
        //                break;
        //            }
        //        case "ChocoStand":
        //            {
        //                ChocoStand.Enabled = true;
        //                ChocoStand.Visible = true;
        //                break;
        //            }
        //        case "SugarFrenzy":
        //            {
        //                SugarFrenzy.Enabled = true;
        //                SugarFrenzy.Visible = true;
        //                break;
        //            }
        //        default:
        //            break;
        //    }
        //    Application.DoEvents(); // Queues the event 
        //}







        // TODO: Get back to this
        //    /// <summary>
        //    /// Supposedly enables the trade buttons to be active if the user qualifies to trade with a certain character.
        //    /// </summary>
        //    /// <param name="ThisTrade">The Trade to be Enabled</param>
        //    public void EnableTheTradeButton(Trade ThisTrade) // TODO: Figure out why this isn't enabling a button
        //    {
        //        List<Button> myButton = btnTrade; // Gets the list of "Trade" buttons that are on the form (5)
        //        foreach (Button myBtn in myButton) // Iterates through each button
        //        {
        //            if ((string)myBtn.Tag == ThisTrade.ID) // Finds the Button's Name/Tag (They're the same) and matches it with the trade that was passed through to here 
        //            {
        //                EnableControl(myBtn); // Function that enables the control (button)
        //            }
        //        }
        //    }
        //    private void EnableControl(Control c)
        //    {
        //        if (c.InvokeRequired) // Checks to see if it's required to invoke something, if true it just goes through this function again
        //        {
        //            c.Invoke(new MethodInvoker (()=> EnableControl(c)));
        //        }
        //        else
        //        {
        //            c.Visible = true; // Makes sure that the button is visible
        //            c.Enabled = true; // Enables the button so the user and trade in the items for the rewards
        //        }
        //    }
        //}
    }
}
 