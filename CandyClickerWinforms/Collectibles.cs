﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;

namespace WindowsFormsApplication1
{
    public partial class Collectibles : Form
    {
        public Collectibles()
        {
            InitializeComponent();

        }

        List<CheckBox> EnableButton = new List<CheckBox>();
        List<Button> ButtonsToEnable = new List<Button>();
        List<Label> UpdatingLabels = new List<Label>();
        private void Collectibles_Load(object sender, EventArgs e)
        {
            GardenItemLabels();
            GoldenItemLabels();
            ChocoItemLabels();
            Buttons();
        }
        private void Buttons()
        {
            Enablers();
            GardenButtons();
            FlowerButtons();
            GoldenItemButtons();
        }
        private void Enablers()
        {
            EnableButton.Add(chkFlower);
            EnableButton.Add(chkGS);
            EnableButton.Add(chkGT);
            EnableButton.Add(chkSculptures);

            chkSculptures.Tag = "SculptureCollectible";
            chkGT.Tag = "GardenCollectible";
            chkGS.Tag = "GoldenCollectible";
            chkFlower.Tag = "ChocoCollectible";
        }
        #region Dynamic Labels
        public void ChocoItemLabels()
        {
            int iCurrentX = 10;
            int iCurrentY = 0;
            int iRowSpacing = 22;
            string[] labelname = { "ChocoCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(120, 18);
                    tabPageChocoStat.Controls.Add(llmf);
                    tabPageChocoStat.AutoScroll = true;

                }
            }

            int zCurrentX = 200;
            int zCurrentY = 0;
            int zRowSpacing = 22;
            string[] qtylabelname = { "ChocoCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    decimal myReqAmt = 0;
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];
                    foreach (KeyValuePair<string, decimal> kvp in iThisItem.PurchaseRequirements)
                        myReqAmt = GameStatus.Singleton.GetQty(kvp.Key);

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lblQty{0}", lmf[i]);
                    llmf.Text = string.Format("{0} / {1}", myReqAmt, GameStatus.Singleton.GetStaticPrice(iThisItem.ID));
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    zCurrentY = zCurrentY + zRowSpacing;
                    llmf.Location = new Point(zCurrentX, zCurrentY);
                    llmf.Size = new System.Drawing.Size(250, 18);

                    UpdatingLabels.Add(llmf);

                    tabPageChocoStat.Controls.Add(llmf);
                    tabPageChocoStat.AutoScroll = true;

                }
            }
        }
        private void GoldenItemLabels()
        {
            int iCurrentX = 10;
            int iCurrentY = 0;
            int iRowSpacing = 22;
            string[] labelname = { "GoldenCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(120, 18);
                    tabPageGolden.Controls.Add(llmf);
                    tabPageGolden.AutoScroll = true;

                }
            }

            int zCurrentX = 200;
            int zCurrentY = 0;
            int zRowSpacing = 22;
            string[] qtylabelname = { "GoldenCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    decimal myReqAmt = 0;
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];
                    foreach (KeyValuePair<string, decimal> kvp in iThisItem.PurchaseRequirements)
                        myReqAmt = GameStatus.Singleton.GetQty(kvp.Key);

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lblQty{0}", lmf[i]);
                    llmf.Text = string.Format("{0} / {1}", myReqAmt, GameStatus.Singleton.GetStaticPrice(iThisItem.ID));
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    zCurrentY = zCurrentY + zRowSpacing;
                    llmf.Location = new Point(zCurrentX, zCurrentY);
                    llmf.Size = new System.Drawing.Size(250, 18);

                    UpdatingLabels.Add(llmf);

                    tabPageGolden.Controls.Add(llmf);
                    tabPageGolden.AutoScroll = true;

                }
            }
        }
        public void SculptureItemLabels()
        {
            int iCurrentX = 10;
            int iCurrentY = 0;
            int iRowSpacing = 22;
            string[] labelname = { "SculptureCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(120, 18);
                    tabPageSculptures.Controls.Add(llmf);
                    tabPageSculptures.AutoScroll = true;

                }
            }

            int zCurrentX = 200;
            int zCurrentY = 0;
            int zRowSpacing = 22;
            string[] qtylabelname = { "SculptureCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    decimal myReqAmt = 0;
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];
                    foreach (KeyValuePair<string, decimal> kvp in iThisItem.PurchaseRequirements)
                        myReqAmt = GameStatus.Singleton.GetQty(kvp.Key);

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lblQty{0}", lmf[i]);
                    llmf.Text = string.Format("{0} / {1}", myReqAmt, GameStatus.Singleton.GetStaticPrice(iThisItem.ID));
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    zCurrentY = zCurrentY + zRowSpacing;
                    llmf.Location = new Point(zCurrentX, zCurrentY);
                    llmf.Size = new System.Drawing.Size(450, 18);
                    tabPageSculptures.Controls.Add(llmf);
                    tabPageSculptures.AutoScroll = true;

                }
            }
        }
        public void GardenItemLabels()
        {

            int iCurrentX = 10;
            int iCurrentY = 0;
            int iRowSpacing = 22;
            string[] labelname = { "GardenCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lbl{0}", lmf[i]);
                    llmf.Text = iThisItem.Name;
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    iCurrentY = iCurrentY + iRowSpacing;
                    llmf.Location = new Point(iCurrentX, iCurrentY);
                    llmf.Size = new System.Drawing.Size(120, 18);
                    tabPageGardening.Controls.Add(llmf);
                    tabPageGardening.AutoScroll = true;

                }
            }

            int zCurrentX = 200;
            int zCurrentY = 0;
            int zRowSpacing = 22;
            string[] qtylabelname = { "GardenCollectible" };
            for (int j = 0; j < labelname.Length; j++)
            {
                List<string> lmf = GameStatus.Singleton.GetItemsInClass(labelname[j]);

                for (int i = 0; i < lmf.Count; i++)
                {
                    decimal myReqAmt = 0;
                    Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[lmf[i]];
                    foreach (KeyValuePair<string, decimal> kvp in iThisItem.PurchaseRequirements)
                        myReqAmt = GameStatus.Singleton.GetQty(kvp.Key);

                    Label llmf = new Label();
                    llmf.Tag = iThisItem.ID;
                    llmf.Name = string.Format("lblQty{0}", lmf[i]);
                    llmf.Text = string.Format("{0} / {1}", myReqAmt, GameStatus.Singleton.GetStaticPrice(iThisItem.ID));
                    llmf.BackColor = Color.Transparent;
                    llmf.Visible = true;
                    zCurrentY = zCurrentY + zRowSpacing;
                    llmf.Location = new Point(zCurrentX, zCurrentY);
                    llmf.Size = new System.Drawing.Size(250, 18);

                    UpdatingLabels.Add(llmf);

                    tabPageGardening.Controls.Add(llmf);
                    tabPageGardening.AutoScroll = true;


                }
            }

        }
        #endregion
        #region Updates

        private void labelUpdate()
        {
            List<Label> myLabels = UpdatingLabels;
            foreach (Label myLbl in myLabels)
            {
                Engine.Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[(string)myLbl.Tag];
                foreach (KeyValuePair<string, decimal> kvp in iThisItem.PurchaseRequirements)
                {
                    decimal myReqAmt = GameStatus.Singleton.GetQty(kvp.Key);
                    myLbl.Text = string.Format("{0} / {1}", myReqAmt, GameStatus.Singleton.GetStaticPrice((string)myLbl.Tag));
                    Update();
                }
            }
        }
        #endregion
        #region Buttons
        #region Garden Buttons
        private void GardenButtons()
        {
                int iCurrentX = 410;
                int iCurrentY = 0;
                int iRowSpacing = 23;

                string[] buttonName = { "GardenCollectible" };
                for (int j = 0; j < buttonName.Length; j++)
                {
                    Button newButton = new Button();
                    newButton.Name = string.Format("button{0}", buttonName[j]);
                    List<string> bmf = GameStatus.Singleton.GetItemsInClass(buttonName[j]);


                    for (int i = 0; i < bmf.Count; i++)
                    {
                        Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[bmf[i]];
                        Button bbmf = new Button();
                        bbmf.Tag = iThisItem.ID;
                        bbmf.Name = "btn" + bbmf.Tag;
                        bbmf.Text = "Collect";
                        
                        bbmf.BackColor = Color.Transparent;
                        bbmf.Visible = true;
                        bbmf.Click += new EventHandler(Item_Click);
                        bbmf.Font = new Font("Georgia", 6);
                        iCurrentY = iCurrentY + iRowSpacing;
                        bbmf.Location = new Point(iCurrentX, iCurrentY);
                        bbmf.Size = new System.Drawing.Size(38, 15);
                        tabPageGardening.Controls.Add(bbmf);
                        bbmf.Enabled = false;
                        bbmf.Visible = true;
                        tabPageGardening.AutoScroll = true;
                        bbmf.BringToFront();
                        ButtonsToEnable.Add(bbmf);
                    }
                }
        }


        #endregion
        #region Flowers
        private void FlowerButtons()
        {
            int iCurrentX = 410;
            int iCurrentY = 0;
            int iRowSpacing = 23;

            string[] buttonName = { "ChocoCollectible" };
            for (int j = 0; j < buttonName.Length; j++)
            {
                Button newButton = new Button();
                newButton.Name = string.Format("button{0}", buttonName[j]);
                List<string> bmf = GameStatus.Singleton.GetItemsInClass(buttonName[j]);


                for (int i = 0; i < bmf.Count; i++)
                {
                    Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[bmf[i]];
                    Button bbmf = new Button();
                    bbmf.Tag = iThisItem.ID;
                    bbmf.Name = "btn" + bbmf.Tag;
                    bbmf.Text = "Collect";

                    bbmf.BackColor = Color.Transparent;
                    bbmf.Visible = true;
                    bbmf.Click += new EventHandler(Item_Click);
                    bbmf.Font = new Font("Georgia", 6);
                    iCurrentY = iCurrentY + iRowSpacing;
                    bbmf.Location = new Point(iCurrentX, iCurrentY);
                    bbmf.Size = new System.Drawing.Size(38, 15);
                    tabPageChocoStat.Controls.Add(bbmf);
                    bbmf.Enabled = false;
                    bbmf.Visible = true;
                    tabPageChocoStat.AutoScroll = true;
                    bbmf.BringToFront();
                    ButtonsToEnable.Add(bbmf);
                }
            }
        }


        #endregion
        #region Golden Items
        private void GoldenItemButtons()
        {
            int iCurrentX = 410;
            int iCurrentY = 0;
            int iRowSpacing = 23;

            string[] buttonName = { "GoldenCollectible" };
            for (int j = 0; j < buttonName.Length; j++)
            {
                Button newButton = new Button();
                newButton.Name = string.Format("button{0}", buttonName[j]);
                List<string> bmf = GameStatus.Singleton.GetItemsInClass(buttonName[j]);


                for (int i = 0; i < bmf.Count; i++)
                {
                    Item iThisItem = GameStatus.Singleton.AllItemsInTheGame[bmf[i]];
                    Button bbmf = new Button();
                    bbmf.Tag = iThisItem.ID;
                    bbmf.Name = "btn" + bbmf.Tag;
                    bbmf.Text = "Collect";

                    bbmf.BackColor = Color.Transparent;
                    bbmf.Visible = true;
                    bbmf.Click += new EventHandler(Item_Click);
                    bbmf.Font = new Font("Georgia", 6);
                    iCurrentY = iCurrentY + iRowSpacing;
                    bbmf.Location = new Point(iCurrentX, iCurrentY);
                    bbmf.Size = new System.Drawing.Size(38, 15);
                    tabPageGolden.Controls.Add(bbmf);
                    bbmf.Enabled = false;
                    bbmf.Visible = true;
                    tabPageGolden.AutoScroll = true;
                    bbmf.BringToFront();
                    ButtonsToEnable.Add(bbmf);
                }
            }
        }
        #endregion

        private bool EnableButtonCheck(string ClassName)
        {
                List<string> myItems = GameStatus.Singleton.GetItemsInClassFromInventory(ClassName);
                List<string> TotalItems = GameStatus.Singleton.GetItemsInClass(ClassName);

                if (myItems.Count >= TotalItems.Count)
                    return true;
                return false;
        }

        private void EnableButtonUpdate()
        {
            List<CheckBox> EnablerButtons = EnableButton;
            foreach (CheckBox myBox in EnablerButtons)
            {
                if (EnableButtonCheck((string)myBox.Tag))
                    myBox.Enabled = true;
            }
        }




        private void SculptureButtonUpdate()
        {
            // Sculptures


            if (GameStatus.Singleton.GetQty("Candy") >= 1000000 && GameStatus.Singleton.GetQty("SwanSculpture") < 1)
            {
                btnSwan.Enabled = true;
            }

            if (GameStatus.Singleton.GetQty("Candy") >= 5000000000 && GameStatus.Singleton.GetQty("MonkeySculpture") < 1)
            {
                btnMonkey.Enabled = true;
            }

            if (GameStatus.Singleton.GetQty("ChocoChips") >= 5000000000 && GameStatus.Singleton.GetQty("DogSculpture") < 1)
            {
                btnDog.Enabled = true;
            }

            if (GameStatus.Singleton.GetQty("SugarCube") >= 5000000000 && GameStatus.Singleton.GetQty("CatSculpture") < 1)
            {
                btnCat.Enabled = true;
            }

            if (GameStatus.Singleton.GetQty("Candy") >= 65000000 && GameStatus.Singleton.GetQty("ChocoChips") >= 65000000
                && GameStatus.Singleton.GetQty("SugarCube") >= 65000000 && GameStatus.Singleton.GetQty("DinoSculpture") < 1)
            {
                btnDinosaur.Enabled = true;
            }

            if (GameStatus.Singleton.GetQty("Candy") >= 1000000000 && GameStatus.Singleton.GetQty("ChocoChips") >= 1000000000
                && GameStatus.Singleton.GetQty("SugarCube") >= 1000000000 && GameStatus.Singleton.GetQty("TrophySculpture") < 1)
            {
                btnTrophy.Enabled = true;
            }
        }

        #endregion

        #region Buttons
        private void btnSwan_Click(object sender, EventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("SwanSculpture");
            btnSwan.Enabled = false;
        }

        private void btnMonkey_Click(object sender, EventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("MonkeySculpture");
            btnMonkey.Enabled = false;
        }

        private void btnDog_Click(object sender, EventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("DogSculpture");
            btnDog.Enabled = false;
        }

        private void btnCat_Click(object sender, EventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("CatSculpture");
            btnCat.Enabled = false;
        }

        private void btnDinosaur_Click(object sender, EventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("DinoSculpture");
            btnDinosaur.Enabled = false;
        }

        private void btnTrophy_Click(object sender, EventArgs e)
        {
            GameStatus.Singleton.AddItemToInventory("TrophySculpture");
            btnTrophy.Enabled = false;
        }
        #endregion

        private void CheckIfEnabled(object sender, EventArgs e)
        {
            Dictionary<CheckBox, Timer> CheckboxToTimer = new Dictionary<CheckBox, Timer>();
            CheckboxToTimer.Add(chkGT, GTTimer);
            CheckboxToTimer.Add(chkSculptures, SculptureTimer);
            CheckboxToTimer.Add(chkFlower, FlowerTimer);
            CheckboxToTimer.Add(chkGS, GSTimer);

            foreach (KeyValuePair<CheckBox, Timer> kvp in CheckboxToTimer)
            {
                if (kvp.Key.Checked)
                    kvp.Value.Start();
                else
                    kvp.Value.Stop();
            }
        }

        private void SculptureTimer_Tick(object sender, EventArgs e)
        {
            GameStatus.Singleton.UpdateInventory("Peanut", 3);
            GameStatus.Singleton.UpdateInventory("Almond", 3);
            GameStatus.Singleton.UpdateInventory("Butter", 3);
            GameStatus.Singleton.UpdateInventory("SoftChoco", 3);
            GameStatus.Singleton.UpdateInventory("MintLeaves", 3);
            GameStatus.Singleton.UpdateInventory("Coffee", 3);

            GameStatus.Singleton.UpdateInventory("Candy", -30000);
            GameStatus.Singleton.UpdateInventory("ChocoChips", -30000);
            GameStatus.Singleton.UpdateInventory("SugarCube", -30000);
        }

        private void GTTimer_Tick(object sender, EventArgs e)
        {
            Program.bClicker.VillainTimer.Enabled = true;
        }

        private void FlowerTimer_Tick(object sender, EventArgs e)
        {
            Program.villain.SuperMinionTimer.Start();
        }

        private void GSTimer_Tick(object sender, EventArgs e)
        {
            MessageBox.Show("You won the game!");
        }

        private void Item_Click(object sender, EventArgs e)
        {
            Button myButton = sender as Button;
            string myItem = (string)myButton.Tag;
            if (GameStatus.Singleton.AbleToBuyTheItem(myItem, 1))
            {
                GameStatus.Singleton.AddItemToInventory(myItem, 1);
                myButton.Enabled = false;
            }
        }

        private void btnEnable()
        {
            List<Button> myButton = ButtonsToEnable;
            foreach (Button btn in myButton)
            {
                string myItem = (string)btn.Tag;
                if (GameStatus.Singleton.AbleToBuyTheItem(myItem, 1))
                    btn.Enabled = true;
            }
        }
        private void Updater_Tick(object sender, EventArgs e)
        {
            EnableButtonUpdate();
            btnEnable();
            labelUpdate();
            SculptureButtonUpdate();
        }
    }
}
