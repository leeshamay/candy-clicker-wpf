﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;

namespace WindowsFormsApplication1
{
    public partial class Villain_ : Form
    {
        public Villain_()
        {
            InitializeComponent();
        }
        // This is the only form that I think doesn't need any fixing/reorganizing/refactoring \m/ +1
        private void Villain__Load(object sender, EventArgs e)
        {
            ControlBox = false;
        }

        public void VillainPH_Click(object sender, EventArgs e)
        {
            var done = new Villain_();
            villainHealth.Value += 1;
            if (Program.collectibles.chkFlower.Checked)
                {
                    SuperMinionTimer.Start();
                }
            if (villainHealth.Value >= 980)
            {
                SuperMinionTimer.Stop();
                VillainPH.Enabled = false;
                btnCollectWin.Enabled = true;
                MessageBox.Show("You win!\nYou win the following:\n30 of each Special Item", "Congrats!", MessageBoxButtons.OK);
            }
        }

        private void btnCollectWin_Click(object sender, EventArgs e)
        {
            this.Close();
            GameStatus.Singleton.UpdateInventory("Peanut", 30);
            GameStatus.Singleton.UpdateInventory("Almond", 30);
            GameStatus.Singleton.UpdateInventory("Coffee", 30);
            GameStatus.Singleton.UpdateInventory("SoftChoco", 30);
            GameStatus.Singleton.UpdateInventory("Butter", 30);
            GameStatus.Singleton.UpdateInventory("MintLeaves", 30);
        }

        private void SuperMinionTimer_Tick(object sender, EventArgs e)
        {
            if (villainHealth.Value < 979)
            {
                villainHealth.Value += 1;
                villainHealth.Update();
                villainHealth.Refresh();
            }
            else
            {
                SuperMinionTimer.Stop();
            }
        }
    }
}
