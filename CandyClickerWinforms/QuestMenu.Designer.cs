﻿namespace WindowsFormsApplication1
{
    partial class MarketMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarketMenu));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.GrandmaStand = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.AuntieStand = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.HoarderStand = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.ChocoStand = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.SugarFrenzy = new System.Windows.Forms.Button();
            this.CheckTrade = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(661, 497);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.richTextBox1);
            this.tabPage1.Controls.Add(this.GrandmaStand);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(653, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Grandma\'s Stall";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(200, 17);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(434, 436);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // GrandmaStand
            // 
            this.GrandmaStand.Location = new System.Drawing.Point(18, 399);
            this.GrandmaStand.Name = "GrandmaStand";
            this.GrandmaStand.Size = new System.Drawing.Size(81, 54);
            this.GrandmaStand.TabIndex = 1;
            this.GrandmaStand.Tag = "GrandmaStand";
            this.GrandmaStand.Text = "Trade!";
            this.GrandmaStand.UseVisualStyleBackColor = true;
            this.GrandmaStand.Click += new System.EventHandler(this.ClickOnTrade);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.richTextBox2);
            this.tabPage2.Controls.Add(this.AuntieStand);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(653, 468);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Auntie\'s Stall";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(200, 17);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(434, 436);
            this.richTextBox2.TabIndex = 3;
            this.richTextBox2.Text = resources.GetString("richTextBox2.Text");
            // 
            // AuntieStand
            // 
            this.AuntieStand.Location = new System.Drawing.Point(18, 399);
            this.AuntieStand.Name = "AuntieStand";
            this.AuntieStand.Size = new System.Drawing.Size(81, 54);
            this.AuntieStand.TabIndex = 1;
            this.AuntieStand.Tag = "AuntieStand";
            this.AuntieStand.Text = "Trade!";
            this.AuntieStand.UseVisualStyleBackColor = true;
            this.AuntieStand.Click += new System.EventHandler(this.ClickOnTrade);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.richTextBox3);
            this.tabPage3.Controls.Add(this.HoarderStand);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(653, 468);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Hoarder\'s Stall";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // richTextBox3
            // 
            this.richTextBox3.Location = new System.Drawing.Point(200, 17);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(434, 436);
            this.richTextBox3.TabIndex = 3;
            this.richTextBox3.Text = resources.GetString("richTextBox3.Text");
            // 
            // HoarderStand
            // 
            this.HoarderStand.Location = new System.Drawing.Point(18, 399);
            this.HoarderStand.Name = "HoarderStand";
            this.HoarderStand.Size = new System.Drawing.Size(81, 54);
            this.HoarderStand.TabIndex = 1;
            this.HoarderStand.Tag = "HoarderStand";
            this.HoarderStand.Text = "Trade!";
            this.HoarderStand.UseVisualStyleBackColor = true;
            this.HoarderStand.Click += new System.EventHandler(this.ClickOnTrade);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.richTextBox4);
            this.tabPage4.Controls.Add(this.ChocoStand);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(653, 468);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Chocolatier\'s Stall";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // richTextBox4
            // 
            this.richTextBox4.Location = new System.Drawing.Point(200, 17);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.ReadOnly = true;
            this.richTextBox4.Size = new System.Drawing.Size(434, 436);
            this.richTextBox4.TabIndex = 3;
            this.richTextBox4.Text = resources.GetString("richTextBox4.Text");
            // 
            // ChocoStand
            // 
            this.ChocoStand.Location = new System.Drawing.Point(18, 399);
            this.ChocoStand.Name = "ChocoStand";
            this.ChocoStand.Size = new System.Drawing.Size(81, 54);
            this.ChocoStand.TabIndex = 1;
            this.ChocoStand.Tag = "ChocoStand";
            this.ChocoStand.Text = "Trade!";
            this.ChocoStand.UseVisualStyleBackColor = true;
            this.ChocoStand.Click += new System.EventHandler(this.ClickOnTrade);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.richTextBox5);
            this.tabPage5.Controls.Add(this.SugarFrenzy);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(653, 468);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Sugar King\'s Stall";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // richTextBox5
            // 
            this.richTextBox5.Location = new System.Drawing.Point(200, 17);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.ReadOnly = true;
            this.richTextBox5.Size = new System.Drawing.Size(434, 436);
            this.richTextBox5.TabIndex = 3;
            this.richTextBox5.Text = resources.GetString("richTextBox5.Text");
            // 
            // SugarFrenzy
            // 
            this.SugarFrenzy.Location = new System.Drawing.Point(18, 399);
            this.SugarFrenzy.Name = "SugarFrenzy";
            this.SugarFrenzy.Size = new System.Drawing.Size(81, 54);
            this.SugarFrenzy.TabIndex = 0;
            this.SugarFrenzy.Tag = "SugarFrenzy";
            this.SugarFrenzy.Text = "Trade!";
            this.SugarFrenzy.UseVisualStyleBackColor = true;
            this.SugarFrenzy.Click += new System.EventHandler(this.ClickOnTrade);
            // 
            // CheckTrade
            // 
            this.CheckTrade.Enabled = true;
            this.CheckTrade.Tick += new System.EventHandler(this.CheckTrade_Tick);
            // 
            // MarketMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 524);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MarketMenu";
            this.Text = "Quests";
            this.Load += new System.EventHandler(this.Market_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button GrandmaStand;
        private System.Windows.Forms.Button AuntieStand;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button HoarderStand;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button ChocoStand;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button SugarFrenzy;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.Timer CheckTrade;
    }
}