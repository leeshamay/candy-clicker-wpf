﻿namespace WindowsFormsApplication1
{
    partial class Villain_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.VillainPH = new System.Windows.Forms.Button();
            this.villainHealth = new System.Windows.Forms.ProgressBar();
            this.btnCollectWin = new System.Windows.Forms.Button();
            this.SuperMinionTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // VillainPH
            // 
            this.VillainPH.Location = new System.Drawing.Point(74, 54);
            this.VillainPH.Name = "VillainPH";
            this.VillainPH.Size = new System.Drawing.Size(351, 462);
            this.VillainPH.TabIndex = 1;
            this.VillainPH.Text = "The Villain hahahahaah";
            this.VillainPH.UseVisualStyleBackColor = true;
            this.VillainPH.Click += new System.EventHandler(this.VillainPH_Click);
            // 
            // villainHealth
            // 
            this.villainHealth.Location = new System.Drawing.Point(40, 584);
            this.villainHealth.Maximum = 980;
            this.villainHealth.Name = "villainHealth";
            this.villainHealth.Size = new System.Drawing.Size(420, 23);
            this.villainHealth.TabIndex = 2;
            // 
            // btnCollectWin
            // 
            this.btnCollectWin.Enabled = false;
            this.btnCollectWin.Location = new System.Drawing.Point(199, 533);
            this.btnCollectWin.Name = "btnCollectWin";
            this.btnCollectWin.Size = new System.Drawing.Size(99, 35);
            this.btnCollectWin.TabIndex = 3;
            this.btnCollectWin.Text = "Collect";
            this.btnCollectWin.UseVisualStyleBackColor = true;
            this.btnCollectWin.Click += new System.EventHandler(this.btnCollectWin_Click);
            // 
            // SuperMinionTimer
            // 
            this.SuperMinionTimer.Enabled = true;
            this.SuperMinionTimer.Tick += new System.EventHandler(this.SuperMinionTimer_Tick);
            // 
            // Villain_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 645);
            this.Controls.Add(this.btnCollectWin);
            this.Controls.Add(this.villainHealth);
            this.Controls.Add(this.VillainPH);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Villain_";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "A Villain Has Appeared!";
            this.Load += new System.EventHandler(this.Villain__Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button VillainPH;
        private System.Windows.Forms.ProgressBar villainHealth;
        private System.Windows.Forms.Button btnCollectWin;
        public System.Windows.Forms.Timer SuperMinionTimer;
    }
}